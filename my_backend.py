"""Prints the environment variables and some Modules API output."""

import webapp2


class MainHandler(webapp2.RequestHandler):

    def get(self):
        self.response.write('hello my backend')


app = webapp2.WSGIApplication([
    ('/my-backend/', MainHandler)
], debug=True)
